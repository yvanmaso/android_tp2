package com.example.maso.myapplication;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

/**
 * Created by maso on 24/03/16.
 */
public class AndroidProvider extends ContentProvider
{

    DatabaseHelper dbHelper;

    public static final Uri CONTENT_URI = Uri
            .parse("content://com.example.maso.myapplication.AndroidProvider");

    // Nom de notre base de données
    public static final String CONTENT_PROVIDER_DB_NAME = "andriodprovider.db";
    // Version de notre base de données
    public static final int CONTENT_PROVIDER_DB_VERSION = 3;
    // Nom de la table de notre base
    public static final String CONTENT_PROVIDER_TABLE_NAME = "client";
    // Le Mime de notre content provider, la premiére partie est toujours identique
    //public static final String CONTENT_PROVIDER_MIME = "vnd.android.cursor.item/vnd.tutos.android.content.provider.client";

    @Override
    public boolean onCreate() {
        dbHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // long id = getId(uri);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return  db.query(AndroidProvider.CONTENT_PROVIDER_TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);

    }

    @Override
    public String getType(Uri uri) {
//      return AndroidProvider.CONTENT_PROVIDER_MIME;
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            long id = db.insertOrThrow(AndroidProvider.CONTENT_PROVIDER_TABLE_NAME, null, values);

            if (id == -1) {
                throw new RuntimeException(String.format(
                        "%s : Failed to insert [%s] for unknown reasons.","AndroidProvider", values, uri));
            } else {
                return ContentUris.withAppendedId(uri, id);
            }

        } finally {
            db.close();
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            return db.delete(AndroidProvider.CONTENT_PROVIDER_TABLE_NAME, selection, selectionArgs);
        } finally {
            db.close();
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            return db.update( AndroidProvider.CONTENT_PROVIDER_TABLE_NAME,values, selection, selectionArgs);
        } finally {
            db.close();
        }
    }


    // Notre DatabaseHelper
    private static class DatabaseHelper extends SQLiteOpenHelper {

        // Création à partir du Context, du Nom de la table et du numéro de version
        DatabaseHelper(Context context) {
            super(context, AndroidProvider.CONTENT_PROVIDER_DB_NAME, null, AndroidProvider.CONTENT_PROVIDER_DB_VERSION);
        }

        // Création des tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + AndroidProvider.CONTENT_PROVIDER_TABLE_NAME + " (" + ColumnsTable.Client.Client_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ColumnsTable.Client.Client_NAME + " VARCHAR(255)," + ColumnsTable.Client.Client_PRENOM + " VARCHAR(255)," + ColumnsTable.Client.Client_DATE + " VARCHAR(255)," + ColumnsTable.Client.Client_VILLE + " VARCHAR(255)" + ");");
        }

        // Cette méthode sert à gérer la montée de version de notre base
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + AndroidProvider.CONTENT_PROVIDER_TABLE_NAME);
            onCreate(db);
        }
    }

}
