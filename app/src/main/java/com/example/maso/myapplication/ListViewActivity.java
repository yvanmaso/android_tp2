package com.example.maso.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by maso on 24/03/16.
 */
public class ListViewActivity extends Activity
{

    // declaration des ListView et Adapters


    private ListView maListView;
    private SimpleAdapter mListAdapter;
    private ArrayList<HashMap<String, String>> listItem;
    private ArrayList<HashMap<String, String>> listItemR;
    static final int IDENT_REQUEST = 1;
    EditText inputSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);
        maListView = (ListView) findViewById(R.id.listviewperso);
        inputSearch = (EditText) findViewById(R.id.EditText01);
        initialisebd();

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // si l'utilisateur change le texte
                if (s.toString().equals("")) {
                    //rénitialisation de la listview
                    initialisebd();
                } else {
                    // lancer la recherche (filtrage par Nom utilisateur)
                    searchItem(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        initialisebd();
    }


    //communication entre les intents
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        /*
        super.onActivityResult(requestCode, resultCode, data);

        Bundle extras = data.getExtras();
        Personne p = extras.getParcelable("pers");

        String prenom = p.getPrenom();
        String nom = p.getNom();
        String date = p.getDate();
        String villee = p.getVille();
        maListView = (ListView) findViewById(R.id.listviewperso);


        //declaration des HashMap
        HashMap<String, String> map;
        if (listItem==null)
        {

            listItem = new ArrayList<HashMap<String, String>>();
        }


       map = new HashMap<String, String>();
       map.put("nom", nom);
        map.put("date", date);
        map.put("ville", villee);
        map.put("img", String.valueOf(R.mipmap.ic_launcher));
        listItem.add(map);

        //Création d'un SimpleAdapter
        if (mListAdapter==null) {
            mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                    new String[]{"nom", "prenom", "date", "ville", "img"}, new int[]{R.id.nom, R.id.prenom, R.id.date, R.id.ville, R.id.img});
            maListView.setAdapter(mListAdapter);
        }

        mListAdapter.notifyDataSetChanged();

*/
    }



    // l'initialisation de la listview à partir de la base de données
    public void initialisebd(){
        String columns[] = new String[] { ColumnsTable.Client.Client_NAME, ColumnsTable.Client.Client_PRENOM, ColumnsTable.Client.Client_DATE, ColumnsTable.Client.Client_VILLE };
        Uri mContacts = AndroidProvider.CONTENT_URI;
        Cursor cur = getContentResolver().query(mContacts, columns, null, null, null);
        listItem = new ArrayList<HashMap<String, String>>();
        mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                new String[]{"nom", "prenom", "date", "ville","img"}, new int[]{R.id.nom, R.id.prenom, R.id.date, R.id.ville,R.id.img});
        maListView.setAdapter(mListAdapter);


        if (cur.moveToFirst())
            do {

                //On déclare la HashMap qui contiendra les informations pour un item
                HashMap<String, String> map;

                map = new HashMap<String, String>();

                map.put("nom", cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_NAME)));
                map.put("prenom", cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_PRENOM)));
                map.put("date", cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_DATE)));
                map.put("ville",cur.getString(cur.getColumnIndex(ColumnsTable.Client.Client_VILLE)));
                map.put("img", String.valueOf(R.mipmap.ic_launcher));
                listItem.add(map);
                if(mListAdapter==null){
                Log.d("e", "enter adapter");
                  mListAdapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                       new String[]{"nom", "prenom", "date", "ville"}, new int[]{R.id.nom, R.id.prenom, R.id.date, R.id.ville});
                maListView.setAdapter(mListAdapter);
                }

                mListAdapter.notifyDataSetChanged();
            } while (cur.moveToNext());
        cur.close();
    }


    public void searchItem(String s){
        listItemR=new ArrayList<HashMap<String, String>>();
        for(HashMap<String, String> mi :listItem){
            listItemR.add(mi);
        }
        for(HashMap<String, String> m :listItemR){
            if(! m.get("nom").contains(s)){

                listItem.remove(m);
            }
        }
        mListAdapter.notifyDataSetChanged();
    }



    public void addItem(View view){
/*
        System.out.print("zezezezezezee______________________");
        Intent client  = new Intent(ListViewActivity.this, MainActivity.class);
        // L'exécution de L'activity MainActivityForm avec l'intent en paramètre.
        startActivityForResult(client, IDENT_REQUEST);
*/

        Intent SecondB = new Intent(ListViewActivity.this, MainActivity.class);
        // démarrer l'activity MainActivityFrom contenant le formulaire
        startActivity(SecondB);

    }
}
