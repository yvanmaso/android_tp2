package com.example.maso.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by maso on 22/03/16.
 */
public class SecondActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seconde_activity);

        Intent act = getIntent();
        Bundle truc = act.getExtras();
       Personne p = truc.getParcelable("pers");

        String prenom = p.getPrenom();
        String nom = p.getNom();
        String date = p.getDate();
        String villee = p.getVille();
        TextView tprenom = (TextView) findViewById(R.id.prenom);
        tprenom.setText("Prenom:"+prenom);
        TextView tnom = (TextView) findViewById(R.id.nom);
        tnom.setText("Nom:" + nom);
        TextView tdate = (TextView) findViewById(R.id.date);
        tdate.setText("Date:"+date);
        TextView tville = (TextView) findViewById(R.id.ville);
        tville.setText("Ville:"+villee);
    }
}
